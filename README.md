# Python 3

Pipeline for Python 3 containers.

file tree:

- base image dockerfile
- list of tags for different runtimes
- directory
  - application name
    - dockerfile which utilizes the above base file
    - README.md that explains what is custom
    - list of tags for different runtimes
